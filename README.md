## Documentation du Pipeline django-single-page

### Schema

![Schema pipeline](/schema/schema.jpeg)

---

### Aperçu

Ce pipeline est conçu pour les projets Python, en particulier ceux utilisant Django. Il est structuré en cinq étapes principales : `setup`, `migrate`, `test`, `build` et `deploy`.

---

### Détails des étapes

#### 1. Setup

- **Objectif** : Préparer l'environnement pour les étapes suivantes.
- **Actions** :
  - Vérification de la version de Python.
  - Création d'un environnement virtuel.
  - Activation de l'environnement virtuel.
  - Mise à jour de pip.
  - Installation de Django.
  - Installation des dépendances à partir de `requirements.txt` (si le fichier existe).
  - Vérification de l'installation de Django.

#### 2. Migrate

- **Objectif** : Appliquer les migrations de la base de données.
- **Actions** :
  - Exécution des migrations avec `python manage.py migrate`.

#### 3. Test

- **Objectif** : Exécuter les tests du projet.
- **Actions** :
  - Exécution des tests avec `python manage.py test`.

#### 4. Build

- **Objectif** : Collecter les fichiers statiques du projet.
- **Actions** :
  - Exécution de `python manage.py collectstatic --noinput`.
  - Conservation des fichiers statiques comme artefacts pour les étapes ultérieures.

#### 5. Deploy

- **Objectif** : Déployer le projet.
- **Actions** :
  - Affichage d'un message indiquant le début du déploiement.
  - Cette étape est exécutée uniquement sur la branche `master`.

---

### Cache

Le pipeline utilise un cache pour le répertoire `venv/` afin d'accélérer les exécutions ultérieures en conservant les dépendances installées.

---

